#' Creation du graphique de l evolution de l artificialisation sur 10 ans en ha des differentes regions.
#' @description Graphique de l evolution de l artificialisation sur 10 ans en ha des differentes regions.
#'
#' @param millesime_obs_artif une année parmi les millesimes sélectionnables par l'utilisateur, au format numerique.
#' @param code_reg code insee de la région sur laquelle construire le graphique
#' @return Un diagramme en barres
#'
#' @importFrom dplyr mutate filter select group_by case_when summarise
#' @importFrom forcats fct_reorder
#' @importFrom ggplot2 ggplot aes geom_bar geom_text position_dodge coord_flip labs theme element_text scale_y_continuous scale_fill_manual scale_color_manual
#' @importFrom glue glue
#' @importFrom scales number_format
#' @importFrom mapfactory format_fr
#' @importFrom attempt stop_if stop_if_not
#'
#' @export
#'
#' @examples
#' creer_graphe_2_3(millesime_obs_artif = 2020,code_reg = "52")
creer_graphe_2_3 <- function(millesime_obs_artif,code_reg = NULL){
  attempt::stop_if(millesime_obs_artif, is.null, msg = "millesime_obs_artif_gk3 n\'est pas renseign\u00e9")
  attempt::stop_if_not(millesime_obs_artif, is.numeric, msg = "millesime_obs_artif_gk3 n\'est pas un nombre")
  attempt::stop_if(code_reg, is.null, msg = "code_reg n\'est pas renseign\u00e9")
  attempt::stop_if_not(code_reg, ~ .x %in% levels(COGiter::regions$REG), msg = "code_reg n\'est pas un code r\u00e9gion valide")

  if (is.numeric(code_reg)) {
    code_reg = as.character(code_reg)
  }

  data <- observatoire_artificialisation %>%
    dplyr::mutate(date = as.character(lubridate::year(.data$date - 1))) %>%
    dplyr::filter(.data$TypeZone == "R\u00e9gions",
                  !(.data$CodeZone %in% c("01","02","03","04","06"))) %>%
    dplyr::select("CodeZone", "TypeZone", "Zone", "date", "flux_naf_artificialisation_total") %>%
    dplyr::mutate(flux_naf_artificialisation_total = .data$flux_naf_artificialisation_total / 10000) %>%
    dplyr::filter(.data$date < millesime_obs_artif, .data$date > millesime_obs_artif - 11) %>% # conserve les 10 derniers millesimes
    dplyr::arrange(.data$Zone) %>%
    dplyr::group_by(.data$CodeZone, .data$TypeZone, .data$Zone) %>%
    dplyr::summarise(`evolution` = sum(.data$flux_naf_artificialisation_total, na.rm = T)) %>%
    dplyr::select("TypeZone", "Zone", "CodeZone", "evolution") %>%
    dplyr::mutate(couleur_barre = dplyr::case_when(
      .data$CodeZone == code_reg ~ 1,
      TRUE ~ 0))

  valeur_max <- max(data$evolution, na.rm = TRUE)
  millesime_debut <- millesime_obs_artif - 10

  graph_2_3<-data  %>%
    ggplot2::ggplot(ggplot2::aes(x=forcats::fct_reorder(.data$Zone,.data$evolution,.desc=F),
                                 y=.data$evolution,
                                 fill=as.factor(.data$couleur_barre))) +
    ggplot2::geom_bar(stat="identity")+
    ggplot2::geom_text(ggplot2::aes(y=.data$evolution ,
                                    label=mapfactory::format_fr(.data$evolution, dec = 0),
                                    color = as.factor(.data$couleur_barre)
    ),
    position= ggplot2::position_dodge(width=1),
    vjust=0.5,
    hjust=-0.5,
    size=3) +
    ggplot2::coord_flip(expand = FALSE) +
    ggplot2::labs(title=glue::glue("Nouvelles surfaces consomm\u00e9es en hectares\npar r\u00e9gion entre le 1/1/{millesime_debut} et le 1/1/{millesime_obs_artif}"),
                  subtitle="",x="",y="",
                  fill="",
                  caption = glue::glue("Source : Observatoire de l\'artificialisation {millesime_obs_artif}"))+
    ggplot2::theme(axis.text.x = ggplot2::element_text(), legend.position = "none") +
    ggplot2::scale_y_continuous(labels = scales::number_format(suffix = "", accuracy = 1, big.mark = " "),limits = c(0, valeur_max+5000)) +
    ggplot2::scale_fill_manual(values = c("#FF8D7E","#82534b")) +
    ggplot2::scale_color_manual(values = c("#FF8D7E","#82534b"))

  return(graph_2_3)

}
