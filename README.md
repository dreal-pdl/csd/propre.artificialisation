
<!-- README.md is generated from README.Rmd. Please edit that file -->

# propre.artificialisation

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![pipeline
status](https://gitlab.com/dreal-datalab/propre.artificialisation/badges/dev/pipeline.svg)](https://gitlab.com/dreal-datalab/propre.artificialisation/-/commits)
[![coverage
report](https://gitlab.com/dreal-datalab/propre.artificialisation/badges/dev/coverage.svg)](https://dreal-datalab.gitlab.io/propre.artificialisation/coverage.html)

<!-- badges: end -->

propre.artificialisation permet de produire une publication régionale
sur l’artificialisation des sols.

Pour l’installer :

``` r
remotes::install_gitlab(repo = "dreal-pdl/csd/propre.artificialisation", 
                        host = "gitlab-forge.din.developpement-durable.gouv.fr")
```

Pour l’utiliser :

![](man/figures/create_rmakdown_propre_artificialisation.gif)<!-- -->

Exemple d’illustration :

``` r
library(propre.artificialisation)
creer_carte_1_7(millesime_ocsge = 2016,
                code_reg = '52')
```

![](man/figures/README-carte_artif-1.png)<!-- -->

# Documentation

-   Documentation de la version de développement :
    <https://dreal-datalab.gitlab.io/propre.artificialisation/dev/index.html>
-   Code coverage du package :
    <https://dreal-datalab.gitlab.io/propre.artificialisation/coverage.html>
