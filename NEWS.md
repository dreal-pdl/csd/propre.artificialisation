# propre.artificialisation 0.0.0.9000

* Création site pkgdown.

* Ajout des 3 vignettes correspondants aux illustrations des 3 chapitres.

* Ajout fonctions d'illustration `creer_carte_1_3()`,`creer_carte_1_7()`,`creer_carte_2_2()`,`creer_carte_2_7()`,`creer_carte_3_2()`, `creer_graphe_1_1()`, `creer_graphe_1_4()`, `creer_graphe_1_5()`,`creer_graphe_1_6()`, `creer_graphe_2_3()`, `creer_graphe_2_4()`, `creer_graphe_2_5()`, `creer_graphe_2_6()`, `creer_graphe_3_1()`, `creer_graphe_3_3()`,`creer_graphe_3_4()`,`creer_graphe_5_2()`,`creer_tableau_1_6()` et leurs tests.

* Ajout template de publication.

* Ajout fichier `NEWS.md` pour tracer les changements dans le package.

